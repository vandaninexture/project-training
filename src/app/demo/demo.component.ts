import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  template: `<nav [style]='navStyle'>
    <a [style.text-decoration]="activeLinkStyle"> Home Page</a>
    <a [style.text-decoration]='linkStyle'>Login</a>
    </nav> <button (click)="onSave()">Save</button>
    <button (myClick)="clickMessage=$event" clickable> Click with myClick</button>
    {{clickMessage}}`,
  styleUrls: ['./demo.component.css']
})

export class DemoComponent implements OnInit {
  title = 'app works!';
  navStyle = 'font-size: 1.2rem; color: blue;';
  linkStyle = 'underline';
  activeLinkStyle = 'overline';
  constructor() { }
  // userName: string = "Vandan";
  clickMessage:any = 'This is demo';
  onSave() {
    alert('Show button clicked !');
  }
  ngOnInit(): void {
  }
}
