import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { HEROES } from './mock-heroes';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private heroesUrl = 'api/heroes';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  /** GET heroes from the server */
  getHeroes(): Observable<Hero[]>{
    // const heroes = of(HEROES);
    // this.messageService.add('HeroService: fetched heroes');
    return this.http.get<Hero[]>(this.heroesUrl)
    .pipe(
      tap(_=> this.log('fetched heroes')),
      catchError(this.handleError<Hero[]>('getHeroes', []))
    );
  }

  getHero(id: number): Observable<Hero> {
    // const hero = HEROES.find(h => h.id === id) as Hero;
    const url = '${this.heroesUrl}/${id}';
    return this.http.get<Hero>(url).pipe(
      tap(_=> this.log('fetched hero id=$(id)')),
      catchError(this.handleError<Hero>('getHero id=$(id)'))
    );
    // this.messageService.add('HeroService: fetched hero id=${id}');
    // return of(hero);
  }

   /** GET hero by id. Return `undefined` when id not found */
   getHeroNo404<Data>(id: number): Observable<Hero> {
     const url = '${this.heroesUrl}/?id=${id}';
     return this.http.get<Hero[]>(url).pipe(
       map(heroes => heroes[0]),
       tap(h => {
         const outcome = h ? 'fetched' : 'did not find';
         this.log('${outcome} hero id=${id}');
       }),
       catchError(this.handleError<Hero>('getHero id=${id}'))
     );
   }

   searchHeroes(term: string): Observable<Hero[]> {
    if(!term.trim()){
      return of([]);
    }
    return this.http.get<Hero[]>('${this.heroesUrl}/?name=${term}').pipe(
      tap(x => x.length ? this.log('found heroes matching "${term}"') :
        this.log('no heroes matching "${term}"')),
      catchError(this.handleError<Hero[]>('searchHeroes',[]))
    );
  }

   /** POST: add a new hero to the server */
  addHero(hero: Hero):Observable<Hero>{
    return this.http.post<Hero>(this.heroesUrl, hero, this.httpOptions).pipe(
      tap((newHero: Hero) => this.log('added hero w/ id=${newHero.id}')),
      catchError(this.handleError<Hero>('addHero'))
    );
  }

  deleteHero(id: number): Observable<Hero> {
    const url = '$(this.heroesUrl}/${id}';

    return this.http.delete<Hero>(url, this.httpOptions).pipe(
      tap(_=> this.log('deleted hero id=${id}')),
      catchError(this.handleError<Hero>('deleteHero'))
    );
  }

  updateHero(hero: Hero): Observable<any> {
    return this.http.put(this.heroesUrl, hero, this.httpOptions).pipe(
      tap(_ => this.log(`updated hero id=${hero.id}`)),
      catchError(this.handleError<any>('updateHero'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log('${operation} failed: ${error.message}');
      return of(result as T);
    };
  }

  private log(message: string){
    this.messageService.add('HeroService: ${message}');
  }


  // getHeroes(){
  //   return [
  //     {
    //       title: 'Angular Http Post Request Example'
    //       id: 1,
  //     },
  //     {
  //       id: 2,
  //       title: 'Angular 10 Routing and Nested Routing Tutorial With Example'
  //     },
  //     {
  //       id: 3,
  //       title: 'How to Create Custom Validators in Angular 10?'
  //     },
  //     {
  //       id: 4,
  //       title: 'How to Create New Component in Angular 10?'
  //     }
  //   ];
  // }
}
